﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pawn : MonoBehaviour
{
    // Declaring a transform variable
    public Transform tf;

    // Variable for starting position or pawns
    public Vector3 startPos;

    // Variable for movement speed
    public float charSpeed;

    // Variable for rotation speed
    public float rotateSpeed;

	// Use this for initialization
	public virtual void Start ()
    {
        // Assigning transform variable
        tf = GetComponent<Transform>();

        // Assign pawns starting position
        startPos = tf.position;
	}
	
	// Update is called once per frame
	public virtual void Update ()
    {
        
    }

    // Move forward function to be inherited
    public virtual void forward()
    {
        
    }

    // Move backward function to be inherited
    public virtual void backward()
    {

    }

    // Rotate left function to be inherited
    public virtual void leftRotate()
    {

    }

    // Rotate right function to be inherited
    public virtual void rightRotate()
    {

    }
}
