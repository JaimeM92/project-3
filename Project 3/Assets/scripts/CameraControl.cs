﻿using UnityEngine;

public class CameraControl : MonoBehaviour
{ 
    // Variable for camera to be assigned to
    public Transform target;

    // Camera move speed
    public float camSpeed;

    // Camera offset - move back
    public Vector3 offset;

    void LateUpdate()
    {
        // Move camera with target movement
        transform.position = target.position + offset;
    }
}
