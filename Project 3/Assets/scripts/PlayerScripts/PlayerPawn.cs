﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPawn : Pawn
{
    // Variables for noise making when player moves
    public Noise pNoise;
    public float moveNoise = 10;
    public float turnNoise = 10;

    // Use this for initialization
    public override void Start ()
    {
        base.Start();
	}
	
	// Update is called once per frame
	public override void Update ()
    {
        base.Update();
	}

    // Inherited function to move player + make noise when moving
    public override void forward()
    {
        tf.transform.Translate(new Vector2(0, charSpeed) * Time.deltaTime);

        if (pNoise != null)
        {
            pNoise.volume = Mathf.Max(pNoise.volume, moveNoise);
        }
    }

    // Inherited function to move player + make noise when moving
    public override void backward()
    {
        tf.transform.Translate(new Vector2(0, -charSpeed) * Time.deltaTime);

        if (pNoise != null)
        {
            pNoise.volume = Mathf.Max(pNoise.volume, moveNoise);
        }
    }

    // Inherited function to rotate player + make noise when moving
    public override void leftRotate()
    {
        tf.transform.Rotate(0, 0, rotateSpeed);
    }

    // Inherited function to rotate player + make noise when moving
    public override void rightRotate()
    {
        tf.transform.Rotate(0, 0, -rotateSpeed);
    }
        
}
