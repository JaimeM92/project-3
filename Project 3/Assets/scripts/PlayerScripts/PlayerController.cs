﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : Controller
{

    // Use this for initialization
    public override void Start ()
    {
        base.Start();
	}

    // Update is called once per frame
    void Update()
    {
        // When pressed, call function
        if (Input.GetKey(KeyCode.W))
        {
            pawn.forward();
        }

        // When pressed, call function
        if (Input.GetKey(KeyCode.S))
        {
            pawn.backward();
        }

        // When pressed, call function
        if (Input.GetKey(KeyCode.A))
        {
            pawn.leftRotate();
        }

        // When pressed, call function
        if (Input.GetKey(KeyCode.D))
        {
            pawn.rightRotate();
        }

        // When pressed, call to Pause State
        if (Input.GetKey(KeyCode.T))
        {
            GameManager.instance.SetGameState((int)GameManager.GState.Pause);
        }
    }
}
