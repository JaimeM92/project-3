﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Noise : MonoBehaviour
{
    // Variables to enable sound heard by enemy
    public float volume = 0;
    public float volumeDecay = 0.17f;
	
	// Update is called once per frame
	void Update ()
    {
        if (volume > 0)
        {
            volume -= volumeDecay;
        }
	}
}
