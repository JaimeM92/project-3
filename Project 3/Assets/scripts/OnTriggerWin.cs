﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnTriggerWin : MonoBehaviour
{
    // Trigger function at end of game to let player win
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
           // Call to Win Screen UI
           GameManager.instance.SetGameState((int)GameManager.GState.Win);
        }
    }
}
