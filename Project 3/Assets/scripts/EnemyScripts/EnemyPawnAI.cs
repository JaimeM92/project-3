﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPawnAI : Pawn
{
    public override void Start()
    {
        base.Start();
    }

    public override void Update()
    {
        base.Update();
    }

    // Allows enemy to move forward
    public override void forward()
    {
        tf.transform.Translate(new Vector3(0, charSpeed * Time.deltaTime, 0));
    }

    // Allows enemy to rotate left
    public override void leftRotate()
    {
        tf.transform.Rotate(0, 0, rotateSpeed * Time.deltaTime);
    }

    // Allows enemy to rotate right
    public override void rightRotate()
    {
        tf.transform.Rotate(0, 0, rotateSpeed * Time.deltaTime);
    }

}
