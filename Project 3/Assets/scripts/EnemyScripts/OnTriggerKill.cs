﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnTriggerKill : MonoBehaviour
{
    // Variable for enemy health
    public int enemyHealth;

    // Variable target for enemy
    public GameObject target;

    // Function to destroy player when colliding
    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.tag == "Player")
        {
            Destroy(other.gameObject);
            GameManager.instance.SetGameState((int)GameManager.GState.Lose);
        }
    }

}
