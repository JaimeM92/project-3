﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySenses : MonoBehaviour
{
    // Variables for senses for enemy AI
    public float sightDis;
    public float viewAngle;
    public float hearingThreshold;

    // Declare variable for Transform
    private Transform tf;

	// Use this for initialization
	void Start ()
    {
        // Assign Transform to variable
        tf = GetComponent<Transform>();
	}

    // Function for enemy to hear player
    public bool HearPlayer(GameObject player)
    {
        // call to noise script, assign value of player noise
        Noise playerNoise = player.GetComponent<Noise>();

        // If no movement, no noise
        if(playerNoise == null)
        {
            return true;
        }

        // Player is making noise
        return false;
    }

    // Function for enemy to spot player
    public bool SeePlayer(GameObject player)
    {
        // Call to player collider
        Collider2D playerCollider = player.GetComponent<Collider2D>();
        if(playerCollider == null)
        {
            return false;
        }

        // Component for transform is assigned to variable
        Transform playerTransform = player.GetComponent<Transform>();
        Vector3 vectorToPlayer = playerTransform.position - tf.position;

        if(Vector3.Angle(vectorToPlayer, tf.right) >= viewAngle)
        {
            return false;
        }

        RaycastHit2D hit = Physics2D.Raycast(tf.position, vectorToPlayer, sightDis);

        // If nothing enetered collider, do nothing
        if(hit.collider == null)
        {
            return false;
        }

        // Player is seen
        else if (hit.collider == playerCollider)
        {
            return true;
        }

        return false;
    }
}
