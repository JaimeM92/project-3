﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFSM : MonoBehaviour
{
    // Call to script and declare variable
    private EnemySenses senses;
    private Pawn ePawn;

    // Delcare enemy States
    public enum EnemyStates
    {
        Idle,
        Chase,
        Look,
        Return,
        MoveTo
    }

    // Declare variables for enemy actions and positions
    public Vector3 startPos;
    public Vector3 targetPoint;
    public EnemyStates eState;
    public float chaseTimer;
    public float proximity;

    Transform tf;

    void Start()
    {
        // Assign values to variables
        senses = GetComponent<EnemySenses>();
        ePawn = GetComponent<Pawn>();
        tf = GetComponent<Transform>();

        startPos = tf.position;
    }

    // Update is called once per frame
    void Update()
    {
        switch (eState)
        {
            // Enemy State of doing nothing
            case EnemyStates.Idle:
                Idle();

                if (senses.HearPlayer(GameManager.instance.player.gameObject))
                {
                    eState = EnemyStates.Look;
                }

                if (senses.SeePlayer(GameManager.instance.player.gameObject))
                {
                    eState = EnemyStates.Chase;
                }
                break;

            // Enemy State to follow player once spotted
            case EnemyStates.Chase:
                Chase();

                if (!senses.SeePlayer(GameManager.instance.player.gameObject))
                {
                    eState = EnemyStates.Look;
                }

                if (Vector3.Distance(tf.position, GameManager.instance.player.tf.position) > chaseTimer)
                {
                    eState = EnemyStates.Return;
                }
                break;

            // Enemy State to have enemy turn to player's location due to noise
            case EnemyStates.Look:
                Look();

                if (senses.SeePlayer(GameManager.instance.player.gameObject))
                {
                    eState = EnemyStates.Chase;
                }

                if (Vector3.Distance(tf.position, GameManager.instance.player.tf.position) > chaseTimer)
                {
                    eState = EnemyStates.Return;
                }
                break;

            // Enemy State to return to original position
            case EnemyStates.Return:
                Return();

                if (senses.HearPlayer(GameManager.instance.player.gameObject))
                {
                    eState = EnemyStates.Look;
                }

                if (senses.SeePlayer(GameManager.instance.player.gameObject))
                {
                    eState = EnemyStates.Chase;
                }

                if (Vector3.Distance(tf.position, startPos) <= proximity)
                {
                    eState = EnemyStates.Idle;
                }
                break;


        }
    }

    // Function for idle
    public void Idle()
    {

    }

    // Function to move enemy to player when noise heard
    public void MoveTo(Vector3 player)
    {
        if (Vector3.Distance(tf.position, startPos) > proximity)
        {
            Vector3 vectorToPlayer = player - tf.position;
            tf.right = vectorToPlayer;

            Move(tf.right);
        }
    }

    // Fucntion to move enemy
    public void Move(Vector3 direction)
    {
        tf.position += (direction.normalized * ePawn.charSpeed);
    }

    // Function to chase player 
    public void Chase()
    {
        targetPoint = GameManager.instance.player.tf.position;
        MoveTo(targetPoint);
    }

    // Function that calls to TurnAround function
    public void Look()
    {
        TurnAround(true);
    }

    // Function to have enemy turn torwards player direction
    public void TurnAround(bool turnClockwise)
    {
        if (turnClockwise)
        {
            ePawn.leftRotate();
        }

        else
        {

            ePawn.rightRotate(); ;
        }
    }

    // Function to have enemy return to starting position
    public void Return()
    {
        targetPoint = startPos;
        MoveTo(targetPoint);
    }
}
