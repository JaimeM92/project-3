﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Restart : MonoBehaviour
{
    // Variable that calls to the scene
    private Scene scene;

    void Start()
    {
        scene = SceneManager.GetActiveScene();
    }

    // Function to reset the game by reseting scene
    public void Reset()
    {
       if(GameManager.instance.mainScreen)
        {
           SceneManager.LoadScene(scene.name);
        }
        else
        {

        }
    }
}
