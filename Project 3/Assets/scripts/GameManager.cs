﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    // Public objects that call to the different UI screens
    public GameObject mainScreen;
    public GameObject gamePlay;
    public GameObject pauseMenu;
    public GameObject winScreen;
    public GameObject loseScreen;

    // Placements for player object
    public PlayerPawn player;
    public GameObject mainPlayer;

    // Singleton
    public static GameManager instance;

    // Declaraing game states  
    public enum GState
    {
        Main,
        Game,
        Pause,
        Win,
        Lose
    }

    // Delcaring variable for game states
    public GState gameState;


    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }

        else
        {
            Destroy(gameObject);
        }
    }

    // Use this for initialization
    void Start ()
    {
        // At start beign state changes
        SetGameState((int)GState.Main);
	}

    // Function for the different state changes
    void StartScreen()
    {
        switch (gameState)
        {
            // Main Menu Screen
            case GState.Main:
                mainScreen.SetActive(true);
                gamePlay.SetActive(false);
                pauseMenu.SetActive(false);
                winScreen.SetActive(false);
                loseScreen.SetActive(false);
                break;

            // Gameplay Screen
            case GState.Game:
                mainScreen.SetActive(false);
                gamePlay.SetActive(true);
                pauseMenu.SetActive(false);
                winScreen.SetActive(false);
                loseScreen.SetActive(false);
                if (mainPlayer == null)
                {
                    gamePlay.GetComponent<Restart>().Reset();
                }
                break;

            // Pause Menu
            case GState.Pause:
                mainScreen.SetActive(false);
                gamePlay.SetActive(false);
                pauseMenu.SetActive(true);
                winScreen.SetActive(false);
                loseScreen.SetActive(false);
                break;

            // Win Screen when player reaches end
            case GState.Win:
                mainScreen.SetActive(false);
                gamePlay.SetActive(false);
                pauseMenu.SetActive(false);
                winScreen.SetActive(true);
                loseScreen.SetActive(false);
                break;

            // Lose screen when player dies
            case GState.Lose:
                mainScreen.SetActive(false);
                gamePlay.SetActive(false);
                pauseMenu.SetActive(false);
                winScreen.SetActive(false);
                loseScreen.SetActive(true);
                break;
        }

    }

    // Function to initiate the game states
    public void SetGameState(int state)
    {
        gameState = (GState)state;
        StartScreen();
    }

    // Function to quite application
    public void Quit()
    {
        Application.Quit();
    }

}
